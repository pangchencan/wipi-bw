module.exports = {
  extends: ["react-app", "react-app/jest", "plugin:prettier/recommended"],
  rules: {
    "no-unused-vars": [
      2,
      {
        // 允许声明未使用变量
        vars: "local",
        // 参数不检查
        args: "none",
      },
    ],
    "prettier/prettier": "warn",
    "no-console": process.env.NODE_ENV === "development" ? "off" : "warn",
    "no-debugger": process.env.NODE_ENV === "development" ? "off" : "error",
  },
};
