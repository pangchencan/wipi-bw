## git

1. 克隆仓库两种协议

- ssh
使用非对称加密实现文件传输
- http
短暂链接的一种协议需要输入用户名和密码 在windows中会保存用户管理凭据中，如果用户名密码修改可以直接修改凭据

```
git clone removeUrl(仓库地址)
```

会在当前目录下生成以仓库名称为名字的文件夹,该文件夹自动跟远程仓库关联，并且受git管理

```
cd 仓库名
```

2. 查看远程仓库地址

```
git remove -v
```

3. 查看文件状态

```
git status
```

远程修改文件

## 冲突
同时修改同一个文件同一行代码时，不确定保留哪次修改就会产生冲突

1. 手动解决冲突从新提交


## 分支
```
git branch //查看本地分支
git branch -a // 查看所有分支
git branch -r // 查看远程分支
git branch <branch name> 新建分支
git checkout <branch name> 切换分支
```


## eslint
作用：javascript代码检测工具 检测js语法比如箭头函数加return 只定义没有修改过值用const定义这种语法规范
1. 新建一个.eslintrc.js文件
2. extends 继承语法包  rules可以自己添加自己的语法配置
3. .eslintignore eslint忽略文件

## bebel
作用：处理js脚本文件。所有js文件要通过babel-loader解析转换成es5语法
1. 判断环境生产环境移除console
2. presets 语法预设
3. plugins babel插件

- 依赖
1. babel-plugin-transform-remove-console  移除console
2. @babel/plugin-proposal-decorators 处理装饰器语法
## prettier 
作用：约定代码风格，比如分号，比如空格，比如缩进
- 依赖
1. preitter
2. eslint-config-prettier 解决eslint和prettier冲突
3. eslint-plugin-prettier eslint编译时结合prettier使用
- 升级eslint@7.28.0

## husky
作用：扩展git hooks 提交之前的动作
新增：.huskyrc
- 依赖
1. husky@4.2.3

## lint-stage
作用：指定文件执行脚本
新增.lintstagedrc
- 依赖
1. lint-staged

## commitlint
检查提交信息格式 git commit -m "信息"
新增commitlint.config.js 配置文件
- 依赖
- @commitlint/cli commitlint指令
- @commitlint/config-conventional  语法配置包

- 提交格式

upd：更新某功能（不是 feat, 不是 fix）

feat：新功能（feature）

fix：修补bug

docs：文档（documentation）

style： 格式（不影响代码运行的变动）

refactor：重构（即不是新增功能，也不是修改bug的代码变动）

test：增加测试

chore：构建过程或辅助工具的变动



### less配置
1. 找到config/webpack.config.js getStyleLoaders方法添加
{
  javascriptEnabled: true,
}
修改
sassRegex =》  lessRegex  1
sassModuleRegex =》 lessModuleRegex 1
sass-loader => less => loader  2

loader配置规则
{
  test://,
  exclude:'',
  use:[{laoder:},{loader:}]
}

less-loader版本问题降到7.x

### 主题颜色配置
antd-theme-generator

1. 新建colorjs文件 通过node执行colorjs文件编译 color.less
2. 在html中引入less,加载less.min.js
3. 在window挂载less对象
4. 切换主题通过window.less.modifyVars方法切换主题
